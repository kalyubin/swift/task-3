
let numbers = [92, 100, 520, 2, 94, 405, 930, 94]

let sortDescNumbers = numbers.sorted(by: { n1, n2 in return n1 > n2})

print(sortDescNumbers)
print()

let sortAscNumbers = numbers.sorted(by: { n1, n2 in return n1 < n2})

print(sortAscNumbers)
print()


class Util{
    var namesArr: [String] = []

    func add(_ names: String...){
        for i in names{
            namesArr.append(i)
        }
        namesArr = namesArr.sorted(by: {(s1: String, s2: String) -> Bool in
            return s1.count < s2.count
        })
    }
}

let nammes = Util()

nammes.add("Timofey", "Svyatoslav", "Ivan", "Andrey", "Ian", "Vladislav")

print(nammes.namesArr)
print()


let dict: Dictionary<Int, String> = [7: "Timofey", 10: "Svyatoslav", 6: "Andrey", 3: "Ian", 9: "Vladislav"]


func outP(_ key: Int){
    guard let val = dict[key] else {
        print("None")
        return 
    }
    print("\(key) - \(val)")
    
}

outP(10)
print()


var a: [String] = []
var b: [Int] = []

func isEmp(_ s: inout [String], _ n: inout [Int]){
    if(s.isEmpty){
        s.append("olkojuh")
        print(s)
        print()
    }
    if(n.isEmpty){
        n.append(17)
        print(n)
        print()
    }
}

isEmp(&a, &b)